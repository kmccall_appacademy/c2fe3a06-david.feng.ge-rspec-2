def reverser
   yield.split.map(&:reverse).join(" ")
   # yield act as the receiver of the block, or the block's parameter
end

def adder(increment = 1)
  yield + increment
end

def repeater(time = 1)
  1.upto(time).each { yield }

end
